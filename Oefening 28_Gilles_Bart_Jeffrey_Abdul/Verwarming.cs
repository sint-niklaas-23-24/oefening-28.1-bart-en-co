﻿namespace Oefening_28_Gilles_Bart_Jeffrey_Abdul
{
     class Verwarming
    {
        private double _graden;
        private bool _power;

        public Verwarming() { }
        public double Graden { get { return _graden; } set { _graden = value; } }
        public bool Power { get { return _power; } set { _power = value; } }

        public double InFahrenheit()
        {
            double F = (Graden * 1.8) + 32;
            return F;
        }
    }
}
