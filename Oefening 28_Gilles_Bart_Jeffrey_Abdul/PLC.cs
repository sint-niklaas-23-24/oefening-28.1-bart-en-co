﻿namespace Oefening_28_Gilles_Bart_Jeffrey_Abdul
{
    class PLC
    {
        //Attributen
        private Verwarming _chauffage;
        private Lichten _salonlichten;

        //Constructors

        //Properties
        public Verwarming Chauffage
        {
            get { return _chauffage; }
            set { _chauffage = value; }
        }

        public Lichten Salonlichten
        {
            get { return _salonlichten; }
            set { _salonlichten = value; }
        }

        //Methoden
        public void DoeLichtenAan()
        {
            Lichten salonlichten = new Lichten();
            salonlichten.Power = true;
        }
        public void DoeLichtenUit()
        {
            Lichten salonlichten = new Lichten();
            salonlichten.Power = false;
        }
        public void PasTemperatuurAan(double graden)
        {
            Verwarming verwarming = new Verwarming();
            graden = verwarming.Graden;
        }
        public void ZetVerwarmingAf()
        {
            Verwarming verwarming = new Verwarming();
            verwarming.Power = false;
        }
        public void ZetVerwarmingOp()
        {
            Verwarming verwarming = new Verwarming();
            verwarming.Power = true;
        }
    }
}
